#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 xxllnc
# SPDX-License-Identifier: EUPL-1.2

import argparse
import minty.config.parser
import os
import psycopg2
import redis
import subprocess
import time
import statsd


def main():
    # Parse arguments
    args = parseopts()

    # Connect to StatsD
    statsd_client = statsd.StatsClient(
        host=os.environ["STATSD_HOST"],
        port=os.environ["STATSD_PORT"],
        prefix=args.statsd_prefix,
    )

    # Deduce the offset used for the filestore table queries
    filestore_offset = 0

    if args.statefulset:
        filestore_offset = args.limit * int(os.uname().nodename.split("-")[:-1])

    # Main application loop
    while True:
        # Connect to Redis
        with statsd_client.timer("config_database_connect_duration"):
            redis_client = redis.Redis(
                os.environ["REDIS_HOST"],
                os.environ["REDIS_PORT"],
            )

        statsd_client.incr("config_database_connect_number")

        # Loop over environments
        for environment_key in redis_client.scan_iter("saas:instance:*"):
            # Get environment configuration from Redis
            environment_name = environment_key.decode("utf-8")

            with statsd_client.timer("config_database_read_duration"):
                environment = redis_client.get(environment_name)

            statsd_client.incr("config_database_read_number")

            print(f"Found environment {environment_name}")

            # Parse configuration
            confparser = minty.config.parser.ApacheConfigParser()
            config = confparser.parse(environment.decode("utf-8"))

            # Extract desired configuration data
            bucket_dir = config["instance"]["storage_bucket"]
            database_url = config["instance"]["zaaksysteemdb.url"]

            # Connect to database
            with statsd_client.timer("database_connect_duration"):
                database_connection = psycopg2.connect(database_url)
                database_cursor = database_connection.cursor()

            statsd_client.incr("database_connect_number")

            filestore_records = db_get_files(
                cursor=database_cursor,
                destination=args.destination[0],
                limit=args.limit,
                offset=filestore_offset,
                statsd=statsd_client,
            )

            # Loop over files
            for record in filestore_records:
                sync_ok = file_sync(
                    destination=args.destination,
                    directory=bucket_dir,
                    origin=args.origin,
                    rclone_config=args.rclone_config,
                    statsd=statsd_client,
                    uuid=record[0],
                )

                if not sync_ok:
                    continue

                # Update row in database
                db_update_record(
                    cursor=database_cursor,
                    destination=args.destination,
                    statsd=statsd_client,
                    uuid=record[0],
                )

            # Clean up handles
            database_cursor.close()
            database_connection.close()

        # Wait for a small amount of time before starting the next run
        time.sleep(args.sleep)


def db_get_files(cursor, destination, limit, offset, statsd):
    cursor.execute(
        """
        SELECT uuid
        FROM filestore
        WHERE NOT (storage_location @> ARRAY[%s])
        LIMIT %s
        OFFSET %s
    """,
        (destination, limit, offset),
    )

    statsd.incr("database_query_number")

    return cursor.fetchall()


def db_update_record(cursor, destination, uuid, statsd):
    database_cursor.execute(
        """
        UPDATE filestore
        SET storage_location = array_append(storage_location, %s)
        WHERE uuid = %s
    """,
        (args.destination, file_uuid),
    )

    statsd.incr("database_query_number")

    return True


def file_sync(uuid, origin, destination, directory, rclone_config, statsd):
    rclone_command = [
        "rclone",
        "--config",
        rclone_config,
        "--ignore-checksum",
        "copy",
        f"{origin}/{directory}/{uuid}",
        f"{destination}/{directory}/{uuid}",
    ]

    # Show rclone command for debugging purposes
    print(" ".join(rclone_command))

    # Run rclone to copy over the file
    with statsd.timer("file_sync_duration"):
        rclone = subprocess.Popen(
            rclone_command,
            stderr=subprocess.PIPE,
        )

    # Log error, if any
    if rclone.returncode is not None and rclone.returncode != 0:
        err = rclone.communicate()

        print(err)

        statsd.incr("file_sync_nok")

        return False

    statsd.incr("file_sync_ok")

    return True


def parseopts():
    argparser = argparse.ArgumentParser(
        "Synchronize filestore to a secondary backup location"
    )
    argparser.add_argument(
        "origin",
        type=str,
    )
    argparser.add_argument(
        "destination",
        type=str,
    )
    argparser.add_argument(
        "--limit",
        default=100,
        help="Limit the amount of files to do per run, per database",
        type=int,
    )
    argparser.add_argument(
        "--rclone-config",
        default="/opt/zaaksysteem/file-syncer/rclone.conf",
        help="Path to the rclone configuration file",
        type=str,
    )
    argparser.add_argument(
        "-s",
        "--sleep",
        default=60,
        help="Seconds to sleep inbetween consecutive runs",
        type=int,
    )
    argparser.add_argument(
        "--statefulset",
        default=False,
        help="Indicate the file-syncer is running as a StatefulSet in Kubernetes",
        type=bool,
    )
    argparser.add_argument(
        "--statsd-prefix",
        default="filesyncer",
        help="The prefix to use for statsd metric names",
        type=str,
    )

    return argparser.parse_args()


if __name__ == "__main__":
    main()
