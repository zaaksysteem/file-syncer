# SPDX-FileCopyrightText: 2022 xxllnc
# SPDX-License-Identifier: EUPL-1.2

FROM debian:11

ARG APP_ROOT=/opt/zaaksysteem/file-syncer

# Ensure all packages are up-to-date
RUN apt-get update \
	&& apt-get upgrade --yes \
	&& apt-get install --yes python3 python3-pip \
	&& apt-get clean autoclean \
	&& apt-get autoremove --yes \
	&& rm -rf /var/lib/{apt,dpkg,cache,log}/

# Copy over application
COPY . "$APP_ROOT/"

# Install all Python dependencies
RUN python3 -mpip install -r "$APP_ROOT/etc/requirements.txt"

# Extend PATH
ENV PATH="$APP_ROOT/bin:$PATH"
